import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'

import { AppComponent } from './app.component';
import { AgreeComponent } from './agree/agree.component';
import { RendererComponent } from './renderer/renderer.component';
import { SymbolFormComponent } from './symbol-form/symbol-form.component';

@NgModule({
  declarations: [
    AppComponent,
    AgreeComponent,
    RendererComponent,
    SymbolFormComponent
  ],
  imports: [
    BrowserModule, FormsModule, HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
