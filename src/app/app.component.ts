import { Component, OnInit } from '@angular/core';
import { Stock } from './models/stock';
import { StockSymbolService } from './stock-symbol.service';
import { from } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Angular';
  oneStock:Stock = new Stock();
  symbolsModel;


  portfolio = [{
    symbol: 'ZZZ',
    price: 250.75,
    qty: 1200
  },{
    symbol: 'XXX',
    price: 350.25,
    qty: 800
  },{
    symbol: 'YYY',
    price: 150.5,
    qty: 1100
  }];
  picUrl = 'https://placehold.it/120x60';
  editing = '';

  selectedSymbol;

  constructor(private stockSymbolService:StockSymbolService) {

  }

  editEventHandler(data) {
    this.editing = data;
  }

  ngOnInit() {
    //Make initial call to service
    this.stockSymbolService.getSymbols().subscribe((results) => {
      this.symbolsModel = results.slice(1,8);
    })
  }

  selectButtonHandler(item) {
    this.selectedSymbol = item;
  }
}
