// Declare a class
export class Stock {
    //class properties
    symbol:string = 'BBB';
    name:string;
    date:string;
    price:number;
    quantity:number;
    isEnabled:boolean;

    // we can declare an optional constructor
    constructor() {

    }
}