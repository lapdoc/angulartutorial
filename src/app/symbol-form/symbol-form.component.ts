import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-symbol-form',
  templateUrl: './symbol-form.component.html',
  styleUrls: ['./symbol-form.component.css']
})
export class SymbolFormComponent implements OnInit {

  symbol = 'ABC';
  qty = 145;
  combined;

  constructor() { }

  ngOnInit() {
    this.combined = `${this.symbol} ${this.qty}`
  }

}
