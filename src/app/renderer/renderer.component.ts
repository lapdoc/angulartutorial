import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-renderer',
  templateUrl: './renderer.component.html',
  styleUrls: ['./renderer.component.css']
})
export class RendererComponent implements OnInit {

  //Declare properties of this component
  @Input() symbol;
  @Input() price;
  @Input() qty;
  @Output() editEvent = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  emitEditEvent(whichSymbol) {
    // Emit a custom event for the parent to handle
    this.editEvent.emit(whichSymbol);
  }

}
